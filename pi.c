#include <stdio.h>
#include "mpi.h"
#include <stdlib.h>
#define MASTER 0 
void srandom (unsigned seed);

int main(int argc,char *argv[]){
	int i,  sumTotal, maxVal;
	int start, end, size, rank;
	float piLocal, piGlobal;

	maxVal = 10000;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	// Defina aqui el segmento que debe procesar una tarea
	// El inicio del segmento en la variable 'start', el fin del segmento
	// la variable 'end'
	srandom (rank);

	piLocal = 0;
	
	float amplitud= (float)maxVal/ (float)size;
	start=amplitud*rank+1;
	end=amplitud*(rank+1);
	if((maxVal-end)<amplitud){//ajuste del ultimo item division inexacta
		end=maxVal;
	}

	
	int circles=0;	
	for(i=0; i<= maxVal; i++){
		//sum = sum +i;
		float x = (float)rand()/(float)RAND_MAX; 
		float y = (float)rand()/(float)RAND_MAX;
		if (x * x + y * y <= 1.0) circles++;
	}

	piLocal= 4.0 * (float) circles/ (float) maxVal;

	MPI_Reduce(&piLocal, &piGlobal,1, MPI_FLOAT, MPI_SUM,0,MPI_COMM_WORLD);

	float w=(float)rand()/ (float)RAND_MAX; 
	// Utilice la funcion 'MPI_Reduce' para guardar en la variable 
	// 'sumTotal' la suma parcial de todos las tareas 
	printf("process %d  circles %d  pilocal %f un x %f \n", rank , circles, piLocal, w);

	if(rank==MASTER){
		float total=(float) piGlobal/ (float) size;
		printf ("\nPi Global: %f\n",total);
	}


	MPI_Finalize();
	
	return 0;
}



