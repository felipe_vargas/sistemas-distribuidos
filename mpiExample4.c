#include <stdio.h>
#include "mpi.h"

int main(int argc,char *argv[]){
	int i, sum, sumTotal, maxVal;
	int start, end, size, rank;

	maxVal = 10000;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	// Defina aqui el segmento que debe procesar una tarea
	// El inicio del segmento en la variable 'start', el fin del segmento
	// la variable 'end'
	sum = 0;

	float amplitud= (float)maxVal/ (float)size;
	start=amplitud*rank+1;
	end=amplitud*(rank+1);
	if((maxVal-end)<amplitud){//ajuste del ultimo item division inexacta
		end=maxVal;
	}
	
	for(i=start; i<= end; i++){
		sum = sum +i;
	}

	MPI_Reduce(&sum, &sumTotal,1, MPI_INT, MPI_SUM,0,MPI_COMM_WORLD);


	// Utilice la funcion 'MPI_Reduce' para guardar en la variable 
	// 'sumTotal' la suma parcial de todos las tareas 
	printf("process %d start %d - end %d \n", rank,start, end );
	printf ("\nTotal: %d\n",sumTotal);

	MPI_Finalize();
	
	return 0;
}



